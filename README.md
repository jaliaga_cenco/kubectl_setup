# Install kubectl

## Manual install

Run these commands

```console
vi script.sh
```

Copy the contents from file `script.sh` on this repository. Then

```console
chmod +x script.sh
./script.sh 
source ~/.bashrc
```

## Using Git

If the instance/node has git installed, run these commands:

```console
git clone https://gitlab.com/jaliaga_cenco/kubectl_setup.git
./kubectl_setup/script.sh
source ~/.bashrc
```

## Shortcuts

- k  --> kubectl
- kc --> kubectx - view available contexts
- kc -c --> view current context
- kn --> kubens  - view available namespaces
- kn -c --> view current namespace
- Autocompletion should be enabled.

