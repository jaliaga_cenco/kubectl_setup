#!/bin/bash

# git clone https://gitlab.com/jaliaga_cenco/kubectl_setup.git 

yum update -y

# vimrc variables

cat > ~/.vimrc << EOL
set number
set tabstop=2
set expandtab
set shiftwidth=2
EOL

echo "prepare environment"
OS="$(uname | tr '[:upper:]' '[:lower:]')"
ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')"
mkdir $HOME/tmp/ && cd $HOME/tmp

echo "install kubectl"
curl -o kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/kubectl
curl -o kubectl.sha256 https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/kubectl.sha256
chmod +x ./kubectl
mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
if grep -q krew ~/.bashrc; then
  echo "export path already added on .bashrc"
  # do nothing
else
  # first time
  echo "export PATH=$PATH:$HOME/bin" >> ~/.bashrc
fi

echo "install krew"
curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.tar.gz"
# if you get an error when downloading krew, try using this URL
# curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/download/v0.4.1/krew.tar.gz"
tar zxvf krew.tar.gz
KREW=./krew-"${OS}_${ARCH}"
"$KREW" install krew
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
if grep -q krew ~/.bashrc; then
  echo "export path already added on .bashrc"
  # do nothing
else
  # first time
  echo "export PATH=${KREW_ROOT:-$HOME/.krew}/bin:$PATH" >> ~/.bashrc
fi

echo "install krew view-secret"
kubectl krew install view-secret
# source https://github.com/elsesiy/kubectl-view-secret
# usage kubectl view-secret <secret>

echo "install kubectx kubens"
git clone https://github.com/ahmetb/kubectx $HOME/tmp/kubectx
cd $HOME/tmp/kubectx && chmod +x kubectx kubens
cp ./kubectx $HOME/bin/kubectx && cp ./kubens $HOME/bin/kubens

pip3 install glances pynvim

echo "restart ur shell, or die"
echo "================== After shell run, execute this"
echo "aws eks update-kubeconfig --name production"
echo "aws eks update-kubeconfig --name <clusterName>"
echo "kubectl krew install ctx ns view-secret use"

# aliases

# create backup
cp ~/.bashrc ~/.bashrc-orig

# if grep kubectx ~/.bashrc returns something do nothing
# else

if grep -q kubectx ~/.bashrc; then
  echo "aliases already added - do nothing"
else
  echo "create aliases"
  echo "alias k=kubectl" >> ~/.bashrc
  echo "complete -F __start_kubectl k" >> ~/.bashrc
  echo "alias kc=kubectx" >> ~/.bashrc
  echo "alias kn=kubens" >> ~/.bashrc
  echo "source <(kubectl completion bash)" >> ~/.bashrc
fi

echo "sleeping"
sleep 2

source ~/.bashrc

